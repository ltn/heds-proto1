<?php

require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use \PhpOffice\PhpSpreadsheet\Cell\Coordinate;

$lastVersion = filemtime("sheet.xlsx");
if ((filemtime('data.txt') < $lastVersion)) {
  $reader = new Xlsx($spreadsheet);
  $spreadsheet = $reader->load("sheet.xlsx");
  $worksheet = $spreadsheet->getActiveSheet();

  $highestRow = $worksheet->getHighestRow();
  $highestColumn = $worksheet->getHighestColumn();
  $highestColumnIndex = Coordinate::columnIndexFromString($highestColumn);

  $firstRow = 2;
  $pathologyIndex = 1;
  $themeIndex = 2;
  $organisationIndex = 3;

  $pathologies = [];
  $themes = [];

  for ($row = $firstRow; $row <= $highestRow; $row++) {
    $pathology = $worksheet->getCellByColumnAndRow($pathologyIndex, $row)->getValue();
    $pathology = strtolower(trim($pathology));
    $organisation = $worksheet->getCellByColumnAndRow($organisationIndex, $row)->getValue();
    $organisation = strtolower(trim($organisation));
    $theme = $worksheet->getCellByColumnAndRow($themeIndex, $row)->getValue();
    $theme = strtolower(trim($theme));

    if ($pathology && !isset($pathologies[$pathology])) {
      $pathologies[$pathology] = array();
    }

    if ($organisation && !isset($organisations[$organisation])) {
      $organisations[$organisation] = array();
    }

    if ($theme && !in_array($theme, $themes)) {
      $themes[] = $theme;
    }

    if ($pathology && $theme && !in_array($theme, $pathologies[$pathology])) {
      $pathologies[$pathology][] = $theme;
    }

    if ($organisation && $theme && !in_array($theme, $organisations[$organisation])) {
      $organisations[$organisation][] = $theme;
    }
    for ($col = 1; $col <= $highestColumnIndex; $col++) {
    }
  }

  sort($themes);

  $data = [
    'pathologies'   => $pathologies,
    'organisations' => $organisations,
    'themes'        => $themes,
  ];
  $data = serialize($data);
  file_put_contents('data.txt', $data);
}

$data          = file_get_contents('data.txt');
$data          = unserialize($data);
$pathologies   = $data['pathologies'];
$organisations = $data['organisations'];
$themes        = $data['themes'];

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
  <title>Moteur de recherche HEDS</title>
  <meta name="description" content="Prototype de moteur de recherche pour HEDS">
  <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
  <!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/light.css'> -->
  <!-- <link rel='stylesheet' href='https://cdn.jsdelivr.net/gh/kognise/water.css@latest/dist/dark.css'> -->
  <style type="text/css" media="screen"></style>

  <!-- development version, includes helpful console warnings -->
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <!-- production version, optimized for size and speed -->
  <!-- <script src="https://cdn.jsdelivr.net/npm/vue"></script> -->

</head>
<body class="max-w-6xl m-auto">
  <div id="wrapper" class="flex flex-col justify-center">
    <div id="header" class="p-5 border-b-2 border-solid">
      <h1 class="text-6xl">Moteur de recherche HEDS</h1>
      <p>Prototype de moteur de recherche pour HEDS</p>
    </div>
    <div id="app-heds">
      <form class="bg-white shadow-md rounded px-8 pt-10 pb-8 mb-4" v-on:submit="cancelEvent($event)">
        <div class="mb-4">
          <label class="block text-gray-700 text-sm font-bold mb-2" for="theme">
            Rechercher des thèmes par thèmes, pathologies ou organisation...
          </label>
          <input v-model="search" v-on:keyup="searchChanged($event)" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="theme" type="text" placeholder="Terme à rechercher">
        </div>
          <template v-if="search && currentThemes.length">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="theme">
              ... puis cliquez sur le thème de votre recherche
            </label>
            <div id="theme-list" class="flex flex-row flex-wrap justify-start" :key="keySearch">
              <span v-for="theme in currentThemes" :key="theme.id" class="shadow border rounded p-1 mr-5 mb-2" :class="{'bg-green-400': theme.selected}" v-on:mouseover="selectTheme(theme, currentThemes)" v-on:mouseleave="selectTheme(false, currentThemes)">{{theme.label}}</span>
            </div>
          </template>
          <template v-if="!search">
            <label class="block text-gray-700 text-sm font-bold mb-2" for="theme">
              ... ou cliquez directement sur le thème de votre recherche
            </label>
            <div id="theme-list" class="flex flex-row flex-wrap justify-start" :key="keySearch">
              <span v-for="(theme, index) in themes" :key="index" class="shadow border rounded p-1 mr-5 mb-2" :class="{'bg-green-400': theme.selected}" v-on:mouseover="selectTheme(theme, themes)" v-on:mouseleave="selectTheme(false, themes)">{{theme.label}}</span>
            </div>
          </template>
          <template v-if="search && !currentThemes.length">
            <span class="italic">Votre recherche n'a donné aucun résultat</span>
          </template>
      </form>

      <span>
        {{message}} {{pathologies.length}} {{themes.length}}
      </span>
    </div>
  </div>

  <script>
    var appHeds = new Vue({
      el: '#app-heds',
      data: {
        timeoutSelect: null,
        currentTheme: null,
        search: null,
        keySearch: 0,
        message: 'You loaded this page on ' + new Date().toLocaleString(),
        themes: [
        <?php
        foreach ($themes as $index => $theme) {
          echo '{';
          echo '"label": "'.$theme.'",';
          echo '"id": "'.$index.'"';
          echo '}';
          if ($index != (sizeof($themes) - 1)) echo ",";
        }
        ?>
        ],
        pathologies: [
        <?php
        $index = 0;
        foreach ($pathologies as $pathology => $themes) {
          echo '{';
          echo '"label": "'.$pathology.'",';
          echo '"themes": [';
          foreach ($themes as $index => $theme) {
            echo '{';
            echo '"label": "'.$theme.'"';
            echo '}';
            if ($index != (sizeof($themes) - 1)) echo ",";
          }
          echo ']';
          echo '}';
          if ($index != (sizeof($pathologies) - 1)) echo ",";
          $index++;
        }
        ?>
        ],
        organisations: [
        <?php
        $index = 0;
        foreach ($organisations as $organisation => $themes) {
          echo '{';
          echo '"label": "'.$organisation.'",';
          echo '"themes": [';
          foreach ($themes as $index => $theme) {
            echo '{';
            echo '"label": "'.$theme.'"';
            echo '}';
            if ($index != (sizeof($themes) - 1)) echo ",";
          }
          echo ']';
          echo '}';
          if ($index != (sizeof($organisations) - 1)) echo ",";
          $index++;
        }
        ?>
        ],
        currentThemes: []
      },
      created() {
      },
      methods: {
        selectTheme: function(theme, themeCollection) {
          if (!theme) {
            this.timeoutSelect = setTimeout(function() {
              for(let i = 0; i < themeCollection.length; i++) {
                themeCollection[i].selected = false;
              }
              this.currentTheme = null;
              this.keySearch++;
            }.bind(this));
            return false;
          }
          clearTimeout(this.timeoutSelect);
          if (this.currentTheme == theme) {
            return false;
          }
          for(let i = 0; i < themeCollection.length; i++) {
            themeCollection[i].selected = false;
            if (themeCollection[i].label.search(theme.label) != -1) {
              themeCollection[i].selected = true;
            }
          }
          theme.selected = true;
          this.keySearch++;
          this.currentTheme = theme;
        },
        searchChanged: function(event) {
          this.currentTheme = null;
          this.currentThemes = [];
          let tempFoundThemes = [];
          this.search = this.search.toLowerCase();
          for(let i = 0; i < this.pathologies.length; i++) {
            if (this.pathologies[i].label.search(this.search) != -1) {
              tempFoundThemes = tempFoundThemes.concat(this.pathologies[i].themes);
            }
          }
          for(let i = 0; i < this.organisations.length; i++) {
            if (this.organisations[i].label.search(this.search) != -1) {
              tempFoundThemes = tempFoundThemes.concat(this.organisations[i].themes);
            }
          }

          for(let i = 0; i < this.themes.length; i++) {
            if (this.themes[i].label.search(this.search) != -1) {
              tempFoundThemes.push(this.themes[i]);
            }
          }

          let themeLabels = [];
          let foundThemes = [];
          for(let i =0; i < tempFoundThemes.length; i++) {
            if (!themeLabels.includes(tempFoundThemes[i].label)) {
              foundThemes.push(tempFoundThemes[i]);
              themeLabels.push(tempFoundThemes[i].label);
            }
          }

          for(let i = 0; i < foundThemes.length; i++) {
            foundThemes[i].id = i;
            foundThemes[i].selected = false;
          }

          this.currentThemes = foundThemes;
        },
        cancelEvent: function(event) {
          event.preventDefault();
        },
      }
    })
  </script>
</body>
</html>
